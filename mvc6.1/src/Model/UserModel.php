<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel{

    protected static $table = 'users';

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (nom,email) VALUES (?,?)",array($post['nom'],$post['email']));
    }
}