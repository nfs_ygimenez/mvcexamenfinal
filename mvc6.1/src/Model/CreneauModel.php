<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel{

    protected static $table = 'creneau';

    public static function getAllCreneaux(){
        return App::getDatabase()->query("SELECT s.title, c.start_at, c.nbrehours, c.id FROM ".self::$table." AS c 
        LEFT JOIN salle AS s ON s.id = c.id_salle
        " ,get_called_class());
    }

    public static function getSalleByCreneauId($id){
        return App::getDatabase()->query("SELECT s.title, s.maxuser, c.start_at, c.nbrehours, c.id FROM ".self::$table." AS c 
        LEFT JOIN salle AS s ON s.id = c.id_salle
        WHERE c.id = $id
        " ,get_called_class());
    }

    public static function getUsersByCreneauId($id){
        return App::getDatabase()->query("SELECT u.id, u.nom, u.email FROM creneau_user AS cu
        LEFT JOIN users AS u ON u.id = cu.id_user
        WHERE cu.id_creneau = $id
        " ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_salle,start_at,nbrehours) VALUES (?,?,?)",array($post['select-salle'],$post['start_at'],$post['nbrehours']));
    }

    public static function insertUser($post,$id){
        App::getDatabase()->prepareInsert("INSERT INTO creneau_user (id_creneau,id_user,created_at) VALUES (?,?,NOW())",array($id,$post['select-user']));
    }

    public static function deleteUserFromCreneau($userid,$creneauid){
        return App::getDatabase()->prepareInsert("DELETE FROM creneau_user WHERE id_user = $userid AND id_creneau = $creneauid",get_called_class(),true);
    }

}