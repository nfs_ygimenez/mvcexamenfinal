<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel{

    protected static $table = 'salle';

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (title,maxuser) VALUES (?,?)",array($post['title'],$post['maxuser']));
    }
}