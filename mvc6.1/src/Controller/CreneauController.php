<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class CreneauController extends AbstractController
{
    private $v;

    public function __construct(){
        $this->v = new Validation();
    }

    public function add(){
        $errors = array();

        $salles = SalleModel::all();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Un nouveau créneau a été ajouté !');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.creneau.add',array(
            'form' => $form,
            'salles' => $salles,
        ));
    }

    public function single($id){
        $errors = array();

        $creneau = $this->getCreneauByIdOr404($id);
        $salle = CreneauModel::getSalleByCreneauId($id);
        $adduser = UserModel::all();
        $users = CreneauModel::getUsersByCreneauId($id);

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validateUser($this->v,$post);
            if($this->v->isValid($errors)) {
                if (count($users) >= $salle[0]->maxuser){
                    $this->addFlash('failure', 'Impossible de réserver car la salle est pleine');
                } else{
                    CreneauModel::insertUser($post,$id);
                    $this->redirect('single-creneau/'.$id);
                }
            }
        }

        $form = new Form($errors);


        $this->render('app.creneau.single', array(
            'form' => $form,
            'creneau' => $creneau,
            'salle' => $salle,
            'adduser' => $adduser,
            'users' => $users,
        ));
    }

    public function delete($userid,$creneauid){
        CreneauModel::deleteUserFromCreneau($userid,$creneauid);
    }

    private function getCreneauByIdOr404($id){
        $creneau = CreneauModel::findById($id);
        if(empty($creneau)) {
            $this->Abort404();
        }
        return $creneau;
    }

    private function validate($v,$post)
    {
        $errors = [];
        $verifSalle = SalleModel::findById($post['select-salle']);
        if (empty($verifSalle)){
            $errors['select-salle'] = 'Cette salle n\'existe pas';
        }

        $today = date('Y-m-d H:i:s');
        if (!empty($post['start_at'])){
            if ($today > $post['start_at']) {
                $errors['start_at'] = 'Le créneau ne peut pas être inférieur à aujourd\'hui';
            }
        } else {
            $errors['start_at'] = 'Veuillez renseigner un créneau';
        }
        $errors['nbrehours'] = $v->numberValid($post['nbrehours'],'nombre',1,72);
        return $errors;
    }

    private function validateUser($v,$post)
    {
        $errors = [];
        $verifUser = UserModel::findById($post['select-user']);
        if (empty($verifUser)){
            $errors['select-user'] = 'Cet utilisateur n\'existe pas';
        }
        return $errors;
    }

}
