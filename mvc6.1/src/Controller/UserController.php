<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class UserController extends AbstractController
{

    private $v;

    public function __construct(){
        $this->v = new Validation();
    }

    public function add(){
        $errors = array();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                UserModel::insert($post);
                $this->addFlash('success', 'Un nouvel utilisateur a été ajouté !');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.user.add',array(
            'form' => $form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 100);
        $errors['email'] = $v->emailValid($post['email'],'email');
        return $errors;
    }



}
