<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class SalleController extends AbstractController
{

    private $v;

    public function __construct(){
        $this->v = new Validation();
    }

    public function add(){
        $errors = array();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                SalleModel::insert($post);
                $this->addFlash('success', 'Une nouvelle salle a été ajoutée !');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.salle.add',array(
            'form' => $form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',2, 255);
        $errors['maxuser'] = $v->numberValid($post['maxuser'], 'nombre',1, 1000);

        return $errors;
    }

}
