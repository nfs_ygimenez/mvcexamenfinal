<section id="frontpage">
    <div class="wrap">
        <div class="liste-users">
            <h2>Liste des utilisateurs</h2>
            <table>
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user){ ?>
                    <tr>
                        <td><?php echo $user->nom; ?></td>
                        <td><?php echo $user->email; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="liste-salles">
            <h2>Liste des salles</h2>
            <table>
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Capacité</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($salles as $salle){ ?>
                    <tr>
                        <td><?php echo $salle->title; ?></td>
                        <td><?php echo $salle->maxuser; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="liste-creneaux">
            <h2>Liste des créneaux</h2>
            <table>
                <thead>
                <tr>
                    <th>Salle</th>
                    <th>Début</th>
                    <th>Durée</th>
                    <th>Détails</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($creneaux as $creneau){ ?>
                    <tr>
                        <td><?php echo $creneau->title; ?></td>
                        <td><?php echo date('d/m/Y à H:i:s',strtotime($creneau->start_at)); ?></td>
                        <td><?php echo $creneau->nbrehours; ?> heures</td>
                        <td><a href="<?php echo $view->path('single-creneau',array('id' => $creneau->id)); ?>">Détails</a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>