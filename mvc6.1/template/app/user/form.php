<form action="" method="post" novalidate>
    <?php echo $form->label('nom','Nom de l\'utilisateur'); ?>
    <?php echo $form->input('nom','text', $user->nom ?? ''); ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('email','Email de l\'utilisateur'); ?>
    <?php echo $form->input('email','email', $user->email ?? ''); ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>