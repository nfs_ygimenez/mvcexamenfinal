<section id="single-creneau">
    <div class="wrap">
        <div class="infos">
            <h2>Salle : <?php echo $salle[0]->title; ?></h2>
            <p>Début le : <?php echo date('d/m/Y à H:i:s',strtotime($creneau->start_at)); ?></p>
            <p>Durée : <?php echo $creneau->nbrehours; ?> heures</p>
            <p>Nombre de places restantes : <?php echo ($salle[0]->maxuser - count($users)); ?> / <?php echo $salle[0]->maxuser; ?></p>
        </div>

        <div class="add_user">
            <form action="" method="post" novalidate>
                <?php echo $form->label('select-user','Choisissez un utilisateur'); ?>
                <?php echo $form->selectEntity('select-user', $adduser, 'nom'); ?>
                <?php echo $form->error('select-user'); ?>

                <?php echo $form->submit('submitted', 'Ajouter'); ?>
            </form>
        </div>

        <div class="liste-users">
            <h2>Utilisateurs présents</h2>
            <table>
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Enlever de la liste</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user){ ?>
                    <tr>
                        <td><?php echo $user->nom; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><a href="<?php echo $view->path('delete-user-creneau',array('userid' => $user->id,'creneauid' => $creneau->id)); ?>">Enlever</a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>