<form action="" method="post" novalidate>
    <?php echo $form->label('select-salle','Choisissez une salle'); ?>
    <?php echo $form->selectEntity('select-salle', $salles, 'title'); ?>
    <?php echo $form->error('select-salle'); ?>

    <?php echo $form->label('start_at','Date et heure de début'); ?>
    <?php echo $form->input('start_at','datetime-local'); ?>
    <?php echo $form->error('start_at'); ?>

    <?php echo $form->label('nbrehours','Durée en heures'); ?>
    <?php echo $form->input('nbrehours','number'); ?>
    <?php echo $form->error('nbrehours'); ?>




    <?php echo $form->submit('submitted', $textButton); ?>
</form>