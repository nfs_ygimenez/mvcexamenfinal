<form action="" method="post" novalidate>
    <?php echo $form->label('title','Nom de la salle'); ?>
    <?php echo $form->input('title','text', $salle->title ?? ''); ?>
    <?php echo $form->error('title');?>

    <?php echo $form->label('maxuser','Nombre maximum de personnes dans la salle'); ?>
    <?php echo $form->input('maxuser','number', $user->email ?? ''); ?>
    <?php echo $form->error('maxuser'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>